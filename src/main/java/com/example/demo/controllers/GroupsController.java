package com.example.demo.controllers;

import com.example.demo.helpers.ActivityGroupCount;
import com.example.demo.helpers.GroupPoint;
import com.example.demo.helpers.PrintGroupStudent;
import com.example.demo.models.*;
import com.example.demo.forms.GroupForm;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.DateUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by MANAR on 14/04/2018.
 */

@Controller
public class GroupsController {
    @Autowired
    private GroupDAO dao;
    @Autowired
    private EventActivityDAO eventActivityDAO;
    @Autowired
    private ActivityDAO activityDAO;
    @Autowired
    private CheckDayDAO checkDayDAO;
    @Autowired
    private CheckStudentDAO checkStudentDAO;
    @Autowired
    private PageDAO pageDAO;
    @Autowired
    private PointAmmountDAO pointAmmountDAO;
    @Autowired
    private StableGroupDAO stableGroupDAO;
    @Autowired
    private CheckPointDAO checkPointDAO;
    @Autowired
    private StudentDAO studentDAO;

    //int year;

    //STEP1
    @RequestMapping("/groups")
    public String index(Model model,@CookieValue("currentCourseId") Course course) {

        Iterable<Group> list =course.getGroups();// dao.findAllByCourseId(currentCourseId);
        model.addAttribute("groups", list);
        return "groups/groupList";
    }

    //STEP2
    @RequestMapping("/groups/update/{id}")
    public String showUpdate(@PathVariable String id, Model model) {
        Group g=dao.findOne(Integer.parseInt(id));
        model.addAttribute("groupForm", new GroupForm());
        model.addAttribute("group",g);
        return "groups/groupsUpdate";
    }

    @PostMapping("/groups/update/{id}")
    public String postUpdate(@PathVariable String id,@ModelAttribute GroupForm groupForm, @RequestParam("file") MultipartFile file) throws Exception {
        Group group=dao.findOne(Integer.parseInt(id));
        group.getStableGroup().setName(groupForm.getName());
        if(file.getBytes().length>0){
            /*URL url =this.getClass().getClassLoader().getResource("static2\\images\\groups");
            String path=url.toURI().getPath()+"\\"+group.getId()+".jpg";*/
            String path=GeneralController.staticPath+"images\\groups\\"+group.getStableGroup().getId()+".jpg";
            File f=new File(path);
            file.transferTo(f);
        }
        stableGroupDAO.save(group.getStableGroup());
        dao.save(group);
        Thread.sleep(1000);
        return "redirect:/groups";
    }

    //STEP3
    @RequestMapping("/groups/add")
    public String groupsForm(Model model) {
        model.addAttribute("groupForm", new GroupForm());
        return "groups/groupsAdd";
    }

    //STEP4
    @PostMapping("/groups")
//    @ResponseBody
    public String groupPost(@ModelAttribute GroupForm groupForm, @RequestParam("file") MultipartFile file,@CookieValue("currentCourseId") Integer currentCourseId) throws IOException, URISyntaxException {
        StableGroup stableGroup=new StableGroup(groupForm.getName());
        Group group = new Group(new Course(currentCourseId),stableGroup);
        //group.setName(groupForm.getName());
        stableGroupDAO.save(stableGroup);
        dao.save(group);
        //String path=new ClassPathResource("/static2").getPath();
        //URL url =this.getClass().getClassLoader().getResource("static2\\images\\groups");
        //String path=url.toURI().getPath()+"\\"+group.getId()+".jpg";
        String path=GeneralController.staticPath+"images\\groups\\"+group.getStableGroup().getId()+".jpg";
        File f=new File(path);
        file.transferTo(f);
        //f.write(file.getBytes());
        return "redirect:/groups";
    }

    @RequestMapping("/groups/delete")
    @ResponseBody
    public void deleteGroup(@RequestParam("groupId") Group group) {

        StudentsController studentsController=new StudentsController();
        List<Student> students=group.getStudents();
        for(Student student:students){
            studentsController.deleteStudent(studentDAO,student);
        }

        /*String path=GeneralController.staticPath+"images\\groups\\"+group.getStableGroup().getId()+".jpg";
        File f=new File(path);
        if(f.delete())*/
        //TODO: (in later versions), delete the stabile group and it's image if there is no group for it
            dao.delete(group);
    }


    //-------------------------activities-----------------------
    @RequestMapping("/activities")
    public String showActivity(Model model,@CookieValue("currentCourseId") Course course) {
        //year =GeneralController.getYear();
        List<Group> groups= course.getGroups();// dao.findAllByCourseId(course.getId());
        List<Activity> activities=  course.getActivities();// activityDAO.findAll();

        /*for (int i=0;i<groups.size();i++) {
            groups.get(i).initEventsLists(eventActivityDAO,activities,year);
        }*/

        model.addAttribute("activities", activities);
        model.addAttribute("groups", groups);
        model.addAttribute("activityGroupCount", new ActivityGroupCount(eventActivityDAO));
        model.addAttribute("year", course.getTitle());
        return "activities";
    }


    @RequestMapping("/activities/add")
    @ResponseBody
    public void addActivity(Model model , @RequestParam("name") String name,@CookieValue("currentCourseId") Course currentCourse) {
        activityDAO.save(new Activity(name,currentCourse));
    }
    @RequestMapping("/activities/addEvent")
    @ResponseBody
    public void addEvent(Model model , @RequestParam("groupId") String groupId,@RequestParam("activityId") String activityId ) {
        Group group=new Group();
        group.setId(Integer.parseInt(groupId));
        Activity activity=new Activity();
        activity.setId(Integer.parseInt(activityId));
        eventActivityDAO.save(new EventActivity(group,activity));
    }

    //-----------absent rate----------------------

    @RequestMapping("/groups/existanceRat")
    public String existanceRate(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        //year =GeneralController.getYear();
        Group.checkDays=currentCourse.getCheckDays();
        //checkDayDAO.findAllByDate1Between(LocalDate.of(year,1,1),LocalDate.of(year,12,31));
        Group.checkStudentDAO=checkStudentDAO;
        List<Group> groups=currentCourse.getGroups();
        // dao.findAllByCourseId(currentCourseId);
        if(groups.size()>0)
        groups.get(0).existenceRat();

        model.addAttribute("groups",currentCourse.getGroups());
        model.addAttribute("year",currentCourse.getTitle());
        return "groups/absentRateGroup";
    }

    //--------------advance rate----------------------

    @RequestMapping("/groups/advanceRate")
    public String advanceRate(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        int year =currentCourse.getStartDate().getYear();
        Group.year=year;
        //Student.pageDAO=pageDAO;
        List<Group> groups=currentCourse.getGroups();
        //dao.findAllByCourseId(currentCourseId);
        if(groups.size()>0)
        groups.get(0).advanceRate();

        model.addAttribute("groups",groups);
        model.addAttribute("year",year);
        return "groups/advanceRateGroup";
    }


    //--------------group points----------------------
    @RequestMapping("/groups/groupPoints")
    public String groupPoints(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        int year =currentCourse.getStartDate().getYear();
        LinkedList<GroupPoint> groupPoints=new LinkedList<>();
        List<Group> groups=currentCourse.getGroups();
        //dao.findAllByCourseId(currentCourseId);
        int points,allPoints=0;
        LocalDate startDate=LocalDate.of(year,1,1);
        LocalDate endDate=LocalDate.of(year,12,31);
        List<Student> students;

        for(Group group:groups){
            points=0;
            students=group.getStudents();
            for(Student student:students){
                Integer p=pointAmmountDAO.getPointsCount(student,startDate,endDate);
                points+=(p==null)? 0:p;
            }
            allPoints+=points;
            groupPoints.add(new GroupPoint(group.getId(),group.getStableGroup().getName(),points));
        }
        model.addAttribute("groupPoints",groupPoints);
        model.addAttribute("allPoints",allPoints);
        model.addAttribute("year",year);
        return "groups/pointstRateGroup";
    }
    //------------------------------------------------------print page---------------------------------------------
    @RequestMapping("/groups/print/{group}")
    public String groupPrint(@PathVariable Group group,Model model,@CookieValue("currentCourseId") Course currentCourse) {

        List<PrintGroupStudent> printGroupStudentList=new ArrayList<>();
        for(Student student:group.getStudents()){
            printGroupStudentList.add(new PrintGroupStudent(student,pointAmmountDAO,checkPointDAO,pageDAO));
        }
        String date=LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        model.addAttribute("printGroupStudentList",printGroupStudentList);
        model.addAttribute("date",date);
        model.addAttribute("group",group);
        return "groups/print-group";
    }
}
