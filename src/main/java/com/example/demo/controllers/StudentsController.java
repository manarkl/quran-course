package com.example.demo.controllers;

import com.example.demo.Setting;
import com.example.demo.forms.PageForm;
import com.example.demo.forms.StudentForm;
import com.example.demo.helpers.PrintGroupStudent;
import com.example.demo.models.*;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by MANAR on 14/04/2018.
 */

@Controller
public class StudentsController {
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private StudentDAO dao;
    @Autowired
    private PageDAO pageDAO;
    @Autowired
    private PartDAO partDAO;
    @Autowired
    private CheckDayDAO checkDayDAO;
    @Autowired
    private CheckStudentDAO checkStudentDAO;
    @Autowired
    private PointAmmountDAO pointAmmountDAO;
    @Autowired
    private StableStudentDAO stableStudentDAO;
    @Autowired
    private CheckPointDAO checkPointDAO;

    //int year;

    //STEP1
    @RequestMapping("/students/{groupId}")
    public String index(@PathVariable String groupId,Model model) {

        Group g=groupDAO.findOne(Integer.parseInt(groupId));
        Iterable<Student> list =g.getStudents();
        model.addAttribute("students", list);
        model.addAttribute("group", g);
        return "students/studentsList";
    }

    @RequestMapping("/students/all")
    public String allStudents(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        Iterable<Student> list =dao.findAllByCourseId(currentCourse.getId());
        model.addAttribute("students", list);
        return "students/allstudents";
    }

    @RequestMapping("/students/show/{id}")
    public String show(@PathVariable String id, Model model,@CookieValue("currentCourseId") Course currentCourse) {
        int year=currentCourse.getStartDate().getYear();

        Student student=dao.findOne(Integer.parseInt(id));
        student.initShowStudentPartsString(partDAO);
        model.addAttribute("student",student);

        LocalDate startDate=LocalDate.of(year,1,1);
        LocalDate endDate=LocalDate.of(year,12,31);



        model.addAttribute("pointsCount",pointAmmountDAO.getPointsCount(student,startDate,endDate));
        model.addAttribute("readPagesCount",pageDAO.countAllByStableStudentAndType1AndDate1Between(student.getStableStudent(),Page.READ_TYPE,startDate,endDate));
        model.addAttribute("absentPagesCount",pageDAO.countAllByStableStudentAndType1AndDate1Between(student.getStableStudent(),Page.ABSENT_TYPE,startDate,endDate));

        //data for existenceRate
        //LocalDate endDate2=LocalDate.of(year,12,31);
        /*Student.checkDays=checkDayDAO.findAllByDate1Between(startDate,endDate2);
        Student.checkStudentDAO=checkStudentDAO;*/



        return "students/studentShow";
    }

    //STEP2
    @RequestMapping("/students/update/{id}")
    public String updateGet(@PathVariable String id, Model model,@CookieValue("currentCourseId") Course currentCourse) {
        Student student=dao.findOne(Integer.parseInt(id));
        Iterable<Group> groups=currentCourse.getGroups();
        Group group1=student.getGroup();
        model.addAttribute("studentForm", new StudentForm());
        model.addAttribute("student",student);
        model.addAttribute("group1",group1);
        model.addAttribute("groups", groups);
        return "students/studentUpdate";
    }

    @PostMapping("/students/update/{id}")
    public String updateStudent(@PathVariable String id,@ModelAttribute StudentForm studentForm, @RequestParam("file") MultipartFile file,@CookieValue("currentCourseId") Integer currentCourseId) throws Exception {
        Student student=dao.findOne(Integer.parseInt(id));
        student.setData(studentForm,currentCourseId);
        Group group = groupDAO.findOne(Integer.parseInt(studentForm.getGroupId()));
        student.setGroup(group);

        if(file.getBytes().length>0){
            /*URL url =this.getClass().getClassLoader().getResource("static2\\images\\students");
            String path=url.toURI().getPath()+"\\"+student.getId()+".jpg";*/
            String path=GeneralController.staticPath+"images\\students\\"+student.getStableStudent().getId()+".jpg";
            File f=new File(path);
            file.transferTo(f);
        }
        stableStudentDAO.save(student.getStableStudent());
        dao.save(student);
        Thread.sleep(1000);
        return "redirect:/students/"+student.getGroup().getId();
    }

    //STEP3
    @RequestMapping("/students/add")
    public String addGet(Model model,@ModelAttribute StudentForm studentForm,@CookieValue("currentCourseId") Integer currentCourseId) {
        Part p2=partDAO.findOne(61);

        Iterable<Group> groups=groupDAO.findAllByCourseId(currentCourseId);
        model.addAttribute("studentForm", new StudentForm());
        model.addAttribute("groups", groups);
        return "students/studentAdd";
    }

    //STEP4
    @PostMapping("/students/add")
//    @ResponseBody
    public String addPost(@ModelAttribute StudentForm studentForm, @RequestParam("file") MultipartFile file,@CookieValue("currentCourseId") Integer currentCourseId) throws Exception {
        Group group = groupDAO.findOne(Integer.parseInt(studentForm.getGroupId()));

        Student student= new Student();
        student.setData(studentForm,currentCourseId);
        student.setGroup(group);
        stableStudentDAO.save(student.getStableStudent());
        dao.save(student);

        //save parts
        Part part;
        Iterable<String> parts= studentForm.getAbsentParts();
        for (String p:parts){
            part=new Part(Byte.parseByte(p),LocalDate.of(0,1,1),(byte) 3,student.getStableStudent(),new PointAmmount(student,pointAmmountDAO));
            partDAO.save(part);
        }
        parts= studentForm.getReadParts();
        for (String p:parts){
            part=new Part(Byte.parseByte(p),LocalDate.of(0,1,1),(byte) 2,student.getStableStudent(),new PointAmmount(student,pointAmmountDAO));
            partDAO.save(part);
        }
        parts= studentForm.getAwqafParts();
        for (String p:parts){
            part=new Part(Byte.parseByte(p),LocalDate.of(0,1,1),(byte) 1,student.getStableStudent(),new PointAmmount(student,pointAmmountDAO));
            partDAO.save(part);
        }

        //save image
        /*URL url =this.getClass().getClassLoader().getResource("static2\\images\\students");
        String path=url.toURI().getPath()+"\\"+student.getId()+".jpg";*/
        String path=GeneralController.staticPath+"images\\students\\"+student.getStableStudent().getId()+".jpg";
        File f=new File(path);
        file.transferTo(f);
        return "redirect:/students/"+student.getGroup().getId();
    }


    @RequestMapping("/students/addPage/{studentId}")
    public String addPage(@PathVariable String studentId,Model model,final HttpServletRequest request,HttpServletResponse response) {
        Student student=dao.findOne(Integer.parseInt(studentId));
        model.addAttribute("student",student);
        model.addAttribute("pageForm",new PageForm());

        final String referer = request.getHeader("referer");
        response.addCookie(new Cookie("returnAddPage",referer));
        return "students/pageAdd";
    }

    @PostMapping("/students/addPage/{student}")
//    @ResponseBody
    public String studentPostPage(@PathVariable Student student,@ModelAttribute PageForm pageForm,@CookieValue("returnAddPage") String returnAddPage,@CookieValue("currentCourseId") Integer currentCourseId) throws Exception {
        Page page=new Page();
        //Student student=dao.findOne(Integer.parseInt(studentId));

        PointAmmount pointAmmount=new PointAmmount(0,student,LocalDate.now());//.getPointAmount(Setting.getSetting(),student,LocalDate.now(),pageForm);

        page.setData(pageForm,student.getStableStudent(),pointAmmount,Setting.getSetting());
        //page.setStableStudent1(student.getStableStudent());
        pointAmmountDAO.save(pointAmmount);
        pageDAO.save(page);
        return "redirect:"+returnAddPage;//"redirect:/students/"+student.getGroup().getId();
    }

    @RequestMapping("/student/page/isExist")
    @ResponseBody
    public int isPageExist(@RequestParam("studentId") Student student ,@RequestParam("pageNum") byte pageNum,@RequestParam("type") byte type) {
        List<Page> pageList=pageDAO.findAllByType1AndNumAndStableStudent(type,pageNum,student.getStableStudent());
        if(pageList.size()>0){
            return 0;
        }
        return 1;
    }


    @RequestMapping("/students/delete")
    @ResponseBody
    public void deleteStudent(@RequestParam("id") Student student) {
        deleteStudent(dao,student);
    }

    public void deleteStudent(StudentDAO studentDAO,Student student){
        /*String path=GeneralController.staticPath+"images\\students\\"+student.getStableStudent().getId()+".jpg";
        File f=new File(path);
        if(f.delete())*/
        //TODO: (in later versions), delete the stabile student and it's image if there is no student for it
        studentDAO.delete(student);
    }

    //-------------check list------------------
   /* @RequestMapping("/students/checkList")
    public String checkList(Model model,@CookieValue("currentCourseId") Integer currentCourseId) {
        List<Group> groups = groupDAO.findAllByCourseIdOrderByIdAsc(currentCourseId);
        model.addAttribute("groups",groups);
        return "students/checkList";
    }*/


/*@PostMapping("/checkList")
    public String checkListPost(HttpServletRequest request,@CookieValue("currentCourseId") Integer currentCourseId) {
        CheckDay checkDay=new CheckDay(LocalDate.now());
        checkDayDAO.save(checkDay);
        String str=request.getParameter("txt29");
        int points;
        List<Student> students=dao.findAllByCourseId(currentCourseId);
        for(Student student:students){
            str=request.getParameter("chk"+student.getId());
            if(str!=null) {
                points = Integer.parseInt(request.getParameter("txt" + student.getId()));
                checkStudentDAO.save(new CheckStudent(student,checkDay));
                pointAmmountDAO.save(new PointAmmount(points,student,checkDay.getDate1()));
            }
        }
        return "redirect:students/absents/"+checkDay.getId();
    }*/
    //ABSENTS list
    @RequestMapping("/students/absents/{checkdayId}")
    public String absentStudents(Model model,@PathVariable String checkdayId,@CookieValue("currentCourseId") Course currentCourse) {
        CheckDay checkDay=checkDayDAO.findOne(Integer.parseInt(checkdayId));
        List<Student> students= checkDayDAO.getAbsentStudents(checkDay,currentCourse);
        String date=checkDay.getDate1().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        model.addAttribute("students",students);
        model.addAttribute("date",date);
        //model.addAttribute("title","الطلاب الغياب");
        return "students/studentsAbsent";
    }

    @RequestMapping("/students/existanceRat")
    public String absentRate(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        List<Student> students= currentCourse.getStudents();// dao.findAllByCourseId(currentCourseId) ;
        int year=currentCourse.getStartDate().getYear();
        //Student.checkDays=checkDayDAO.findAllByDate1Between(LocalDate.of(year,1,1),LocalDate.of(year,12,31));
        //Student.checkStudentDAO=checkStudentDAO;

        model.addAttribute("students",students);
        model.addAttribute("year",year);
        //model.addAttribute("title","الطلاب الغياب");
        return "students/absentRateStudent";
    }
//---------------------------------------------version 2 -----------------------------------------------------------------------------------------
//--------------------------------------------version2---------------------------

    @RequestMapping("/student/existNum")
    public String existNum(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        LocalDate date=LocalDate.now();
        String dateStr=date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        CheckDay checkDay=checkDayDAO.findByDate1Is(date);
        if(checkDay==null){
            checkDay=new CheckDay(date,currentCourse);
            checkDayDAO.save(checkDay);
        }

        model.addAttribute("date",dateStr);
        model.addAttribute("checkDayId",checkDay.getId());
        return "students/studentsExistNum";
    }

    @RequestMapping("/student/existName/{id}")
    public String existName(@PathVariable Integer id, Model model,@CookieValue("currentCourseId") Integer currentCourseId) {
        Student student=dao.findByStableStudentIdAndCourseId(id,currentCourseId);
    //String date=checkDay.getDate1().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

    model.addAttribute("student",student);
    //model.addAttribute("date",date);
    return "students/studentsExistName";
}

    @RequestMapping("/student/setExist/{id}")
    public String setExist(@PathVariable String id, Model model,@CookieValue("currentCourseId") Course currentCourse) {
        LocalDate date=LocalDate.now();
        CheckDay checkDay=checkDayDAO.findByDate1Is(date);
        if(checkDay==null){
            checkDay=new CheckDay(date,currentCourse);
        checkDayDAO.save(checkDay);
        }
        Student student=new Student(Integer.parseInt(id));


        /*List<CheckStudent> checkStudents=checkStudentDAO.findAllByStudentAndCheckDay(student,checkDay);
        if(checkStudents.size()==0)*/
        PointAmmount pointAmmount=new PointAmmount(Setting.getSetting().getDayPoints(),student,checkDay.getDate1());
        pointAmmountDAO.save(pointAmmount);
            checkStudentDAO.save(new CheckStudent(student,checkDay,pointAmmount));


          /*  //done:delete this. must find a solution and delete this (the problem of 31/12)
            if(date.isEqual(LocalDate.of(date.getYear(),12,31)))
                checkStudentDAO.save(new CheckStudent(0,student,checkDay));*/

        return "redirect:/student/existNum";
    }

    @RequestMapping("/student/isExist")
    @ResponseBody
    public int isStudentExist(@RequestParam("studentId") Student student) {
        CheckDay checkDay=checkDayDAO.findByDate1Is(LocalDate.now());
        if(checkDay==null){
            return 1;
        }
        List<CheckStudent>checkDayList= checkStudentDAO.findAllByStudentAndCheckDay(student,checkDay);
        if(checkDayList.size()>0){
            return 0;
        }
        return 1;
    }
//-------------------------------points---------------------------------------
    @RequestMapping("/student/pointsNum")
    public String pointsNum(Model model) {
        LocalDate date=LocalDate.now();
        String dateStr=date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        model.addAttribute("date",dateStr);
        return "students/studentsPointsNum";
    }

    @RequestMapping("/student/pointsName/{id}")
    public String pointsName(@PathVariable Integer id, Model model, @CookieValue("currentCourseId") Integer currentCourseId) {
        Student student=dao.findByStableStudentIdAndCourseId(id,currentCourseId);
        //String date=checkDay.getDate1().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        model.addAttribute("student",student);
        //model.addAttribute("date",date);
        return "students/studentsPointsName";
    }

    @RequestMapping("/student/setPoints/{id}/{points}")
    public String setpoints(@PathVariable String id,@PathVariable int points, Model model) {
        LocalDate date=LocalDate.now();
        Student student=new Student(Integer.parseInt(id));
        pointAmmountDAO.save(new PointAmmount(points,student,date));
        /*List<CheckStudent> checkStudents=checkStudentDAO.findAllByStudentAndCheckDay(student,checkDay);
        if(checkStudents.size()==0)*/
        return "redirect:/student/pointsNum";
    }

    @RequestMapping("/students/print/{student}")
    public String print(@PathVariable Student student, Model model,@CookieValue("currentCourseId") Course currentCourse) {
        //Student student=dao.findOne(Integer.parseInt(studentId));
        int year=currentCourse.getStartDate().getYear();
        LocalDate startDate=LocalDate.of(year,1,1);
        LocalDate endDate=LocalDate.of(year,12,31);
        int absentPartsCount=partDAO.findAllByType1AndStableStudentAndDate1BetweenOrderByNumAsc(Part.absent,student.getStableStudent(),startDate,endDate).size();


        model.addAttribute("student",student);
        model.addAttribute("pointsCount",pointAmmountDAO.getPointsCount(student,startDate,endDate));
        model.addAttribute("absentPartsCount",absentPartsCount);
        model.addAttribute("absentPagesCount",pageDAO.countAllByStableStudentAndType1AndDate1Between(student.getStableStudent(),Page.ABSENT_TYPE,startDate,endDate));
        return "students/print-student";
    }

//----------------------------------------checks--------------------------------------------------------------------

    @RequestMapping("/student/checkNum")
    public String checkNum(Model model) {
        LocalDate date=LocalDate.now();
        String dateStr=date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        model.addAttribute("date",dateStr);
        return "students/check-num-student";
    }

    @RequestMapping("/student/checkName")
    public String checkName(@RequestParam("studentId1") int studentId1,@RequestParam("studentId2") int studentId2,@RequestParam("studentId3") int studentId3, Model model,@CookieValue("currentCourseId") Integer currentCourseId) {

        Student student1=dao.findByStableStudentIdAndCourseId(studentId1,currentCourseId);
        Student student2=dao.findByStableStudentIdAndCourseId(studentId2,currentCourseId);
        Student student3=dao.findByStableStudentIdAndCourseId(studentId3,currentCourseId);
        PrintGroupStudent printGroupStudent1;
        PrintGroupStudent printGroupStudent2;
        PrintGroupStudent printGroupStudent3;

        if(student1==null){
            student1=new Student(-1,"");
            printGroupStudent1=new PrintGroupStudent(student1);
        }else {
            printGroupStudent1=new PrintGroupStudent(student1,pointAmmountDAO,checkPointDAO);
        }
        if(student2==null){
            student2=new Student(-1,"");
            printGroupStudent2=new PrintGroupStudent(student2);
        }else {
            printGroupStudent2=new PrintGroupStudent(student2,pointAmmountDAO,checkPointDAO);
        }
        if(student3==null){
            student3=new Student(-1,"");
            printGroupStudent3=new PrintGroupStudent(student3);
        }else {
            printGroupStudent3=new PrintGroupStudent(student3,pointAmmountDAO,checkPointDAO);
        }

        model.addAttribute("printGroupStudent1",printGroupStudent1);
        model.addAttribute("printGroupStudent2",printGroupStudent2);
        model.addAttribute("printGroupStudent3",printGroupStudent3);
        //model.addAttribute("date",date);
        return "students/check-name-student";
    }

    @RequestMapping("/student/setCheck")
    public String setCheck(@RequestParam("studentId1") int studentId1,@RequestParam("studentId3") int studentId3,@RequestParam("studentId2") int studentId2,
                            @RequestParam("points1")int points1,@RequestParam("points2")int points2,@RequestParam("points3")int points3) {
        LocalDate date=LocalDate.now();
        Student student1=dao.findOne(studentId1);
        Student student2=dao.findOne(studentId2);
        Student student3=dao.findOne(studentId3);

        CheckPoint checkPoint1;
        CheckPoint checkPoint2;
        CheckPoint checkPoint3;

        if(student1==null) {
            student1 = new Student("");
            checkPoint1=new CheckPoint(-1,student1,date);
        }else{
            checkPoint1=new CheckPoint(points1,student1,date);
            checkPointDAO.save(checkPoint1);
        }

        if(student2==null) {
            student2 = new Student("");
            checkPoint2=new CheckPoint(-1,student2,date);
        }else{
            checkPoint2=new CheckPoint(points2,student2,date);
            checkPointDAO.save(checkPoint2);
        }

        if(student3==null) {
            student3 = new Student("");
            checkPoint3=new CheckPoint(-1,student3,date);
        }else{
            checkPoint3=new CheckPoint(points3,student3,date);
            checkPointDAO.save(checkPoint3);
        }

       /* request.setAttribute("studentId1",student1.getId());
        request.setAttribute("points1",points1);
        request.setAttribute("studentId1",student1.getId());
        request.setAttribute("points1",points1);
        request.setAttribute("studentId1",student1.getId());
        request.setAttribute("points1",points1);*/

        /*model.addAttribute("date",date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        model.addAttribute("checkPoint1",checkPoint1);
        model.addAttribute("checkPoint2",checkPoint2);
        model.addAttribute("checkPoint3",checkPoint3);*/
        String parms="?points1="+points1+"&points2="+points2+"&points3="+points3+"&studentId2="+studentId2+"&studentId1="+studentId1+"&studentId3="+studentId3;
        return "redirect:/student/print-check"+parms;
    }

    @RequestMapping("/student/print-check")
    public String printCheck(@RequestParam("studentId1") int studentId1,@RequestParam("studentId3") int studentId3,@RequestParam("studentId2") int studentId2,
                           @RequestParam("points1")int points1,@RequestParam("points2")int points2,@RequestParam("points3")int points3, Model model) {
        LocalDate date=LocalDate.now();
        Student student1=dao.findOne(studentId1);
        Student student2=dao.findOne(studentId2);
        Student student3=dao.findOne(studentId3);

        CheckPoint checkPoint1;
        CheckPoint checkPoint2;
        CheckPoint checkPoint3;

        if(student1==null) {
            student1 = new Student("");
            checkPoint1=new CheckPoint(-1,student1,date);
        }else{
            checkPoint1=new CheckPoint(points1,student1,date);
        }

        if(student2==null) {
            student2 = new Student("");
            checkPoint2=new CheckPoint(-1,student2,date);
        }else{
            checkPoint2=new CheckPoint(points2,student2,date);
        }

        if(student3==null) {
            student3 = new Student("");
            checkPoint3=new CheckPoint(-1,student3,date);
        }else{
            checkPoint3=new CheckPoint(points3,student3,date);
        }

        model.addAttribute("date1",date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        model.addAttribute("checkPoint1",checkPoint1);
        model.addAttribute("checkPoint2",checkPoint2);
        model.addAttribute("checkPoint3",checkPoint3);

        return "students/pprint-check";
    }
}
