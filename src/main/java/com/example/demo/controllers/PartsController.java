package com.example.demo.controllers;

import com.example.demo.Setting;
import com.example.demo.forms.PageForm;
import com.example.demo.forms.StudentForm;
import com.example.demo.models.*;
import com.example.demo.repository.*;
import jdk.nashorn.internal.runtime.Undefined;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by MANAR on 14/04/2018.
 */

@Controller
public class PartsController {
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private StudentDAO dao;
    @Autowired
    private PartDAO partDAO;
    @Autowired
    private CheckDayDAO checkDayDAO;

    @Autowired
    private PointAmmountDAO pointAmmountDAO;

    int year;


    //------------parts---------------------------------------------

    @RequestMapping("/part/awqafAll")
    public String awqafAll(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        year =currentCourse.getStartDate().getYear();
        List<Student> students= currentCourse.getStudents();// dao.findAllByCourseId(currentCourse.getId());
        for (Student student:students) {
            student.initAllPartsString((byte) 1,partDAO,year);
        }

        model.addAttribute("students",students);
        model.addAttribute("year",year);
        model.addAttribute("title","سبر الأوقاف");
        model.addAttribute("type","1");
        return "students/partsAll";
    }

    @RequestMapping("/part/awqaf/{groupId}")
    public String awqafGroup(Model model,@PathVariable String groupId,@CookieValue("currentCourseId") Course currentCourse) {
        year =currentCourse.getStartDate().getYear();
        Group group= groupDAO.findOne(Integer.parseInt(groupId));
        List<Student> students= group.getStudents();
        for (Student student:students) {
            student.initAllPartsString((byte) 1,partDAO,year);
        }

        model.addAttribute("students",students);
        model.addAttribute("year",year);
        model.addAttribute("title","سبر الأوقاف لحلقة : "+ group.getStableGroup().getName());
        model.addAttribute("type","1");
        return "students/partsAll";
    }

    @RequestMapping("/part/honoring")
    public String partHonoring(Model model,@CookieValue("currentCourseId") Course currentCourse) {
        year=currentCourse.getStartDate().getYear();
        List<Student> students= dao.findAllByCourseId(currentCourse.getId());
        for (Student student:students) {
            student.initAllPartsString((byte) 4,partDAO,year);
        }

        model.addAttribute("students",students);
        model.addAttribute("year",year);
        model.addAttribute("title","التكريم السنوي");
        model.addAttribute("type","4");
        return "students/partsAll";
    }

    @RequestMapping("/part/add")
    @ResponseBody
    public void partAdd(@RequestParam("studentId") Student student ,@RequestParam("partNum") byte partNum,@RequestParam("type") byte type,@CookieValue("currentCourseId") int currentCourse) {
        //Student student=dao.findByStableStudentIdAndCourseId(stableStudentId,currentCourse);


        Part part=new Part();
        part.setType1(type);
        part.setStableStudent(student.getStableStudent());
        part.setNum(partNum);
        part.setDate1(LocalDate.now());

        PointAmmount pointAmmount=PointAmmount.getForPart(student,type);
        pointAmmountDAO.save(pointAmmount);
        part.setPointAmmount(pointAmmount);

        partDAO.save(part);
       /* //your controller code
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;*/

    }

    @RequestMapping("/part/isExist")
    @ResponseBody
    public int isPartExist(@RequestParam("studentId") Student student ,@RequestParam("partNum") byte partNum,@RequestParam("type") byte type) {
        List<Part> partList=partDAO.findAllByType1AndNumAndStableStudent(type,partNum,student.getStableStudent());
        if(partList.size()>0){
            return 0;
        }
        return 1;
    }



    @RequestMapping("/part/addAwqaf/{studentId}")
    public String addAwqafPart(Model model,@PathVariable String studentId) throws Exception {
        model.addAttribute("studentId",studentId);
        return "students/addAwqafPart";
    }

    @PostMapping("/part/addAwqaf/{student}")
    public String postAwqafPart(@PathVariable Student student, @RequestParam("date2") String date2, @RequestParam("file") MultipartFile file, @RequestParam("partNum") String partNum,@CookieValue("currentCourseId") int currentCourse) throws Exception {
        //Student student=dao.findByStableStudentIdAndCourseId(studentID,currentCourse);
        LocalDate date = LocalDate.parse(date2, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        //Student student=new Student();
        //student.setId(studentId);

        PointAmmount pointAmmount=new PointAmmount(Setting.getSetting().getPartAwqaf(),student,LocalDate.now());
        pointAmmountDAO.save(pointAmmount);

        Part awqafPart=new Part(Byte.parseByte(partNum),date,Part.awqaf,student.getStableStudent(),pointAmmount);
        partDAO.save(awqafPart);


        /*URL url =this.getClass().getClassLoader().getResource("static2\\images\\awqafParts");
        String path=url.toURI().getPath()+"\\"+awqafPart.getId()+".jpg";*/
        String path=GeneralController.staticPath+"images\\awqafParts\\"+awqafPart.getId()+".jpg";
        File f=new File(path);
        file.transferTo(f);

        return "redirect:/part/awqafAll";
    }

    @RequestMapping("/part/addHonoring/{studentId}")
    public String addHonoringPart(Model model,@PathVariable String studentId) throws Exception {
        model.addAttribute("studentId",studentId);
        return "students/addHonoringPart";
    }

    @PostMapping("/part/addHonoring/{student}")
    public String postHonoringPart(@PathVariable Student student, @RequestParam("date2") String date2, @RequestParam("partNum") String partNum,@CookieValue("currentCourseId") int currentCourse) throws Exception {
        //Student student=dao.findByStableStudentIdAndCourseId(studentID,currentCourse);
        LocalDate date = LocalDate.parse(date2, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        PointAmmount pointAmmount=new PointAmmount(0,student,LocalDate.now());
        pointAmmountDAO.save(pointAmmount);

        Part awqafPart=new Part(Byte.parseByte(partNum),date,Part.honoring,student.getStableStudent(),pointAmmount);
        partDAO.save(awqafPart);

        return "redirect:/part/honoring";
    }
}
