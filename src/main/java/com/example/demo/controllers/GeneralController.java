package com.example.demo.controllers;

import com.example.demo.DemoApplication;
import com.example.demo.Setting;
import com.example.demo.forms.SettingsForm;
import com.example.demo.models.*;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by MANAR on 14/04/2018.
 */

@Controller
public class GeneralController {
    @Autowired
    private CheckDayDAO checkDayDAO;
    @Autowired
    private CourseDAO courseDAO;
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private StudentDAO studentDAO;
    @Autowired
    private FileArchiveDAO fileArchiveDAO;



    //private static int year=-1;

    public static String staticPath="";


    @PostConstruct
    void init() throws Exception{
        //FileHandler.checkValidity();
        String url="http://localhost:6115";

        if(Desktop.isDesktopSupported()){
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI(url));
                DemoApplication.logo.end();
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        }else{
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("rundll32 url.dll,FileProtocolHandler " + url);
                DemoApplication.logo.end();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @RequestMapping("/")
    public String home(Model model,HttpServletResponse response){
        setCurrentCourseId(response,courseDAO);
        return "redirect:/index";
    }

    @RequestMapping("/index")
    public String index(Model model,HttpServletResponse response,HttpServletRequest request,@CookieValue(value = "currentCourseId",defaultValue = "-1") Course course) {
        //String year2= WebUtils.getCookie(request,"year").getValue();
        //year=getYear();
        String year=course.getTitle();
            //return "redirect:/years";
        model.addAttribute("year",year);
        return "index";
    }

    /*@RequestMapping("/years")
    public String years(Model model) {
        //List<String> years=checkDayDAO.getyearss();
        List<CheckDay> days=checkDayDAO.findAll();
        Map<Integer,Integer> daysCount= new HashMap<Integer,Integer>();
        List<Integer> years= new ArrayList<>();
        boolean containsCurrentYear=false;
        for(CheckDay day:days){
            if(!daysCount.containsKey(day.getDate1().getYear())) {
                daysCount.put(day.getDate1().getYear(), 0);
                years.add(day.getDate1().getYear());
                if(!containsCurrentYear)
                    containsCurrentYear=LocalDate.now().getYear()==day.getDate1().getYear();
            }
            daysCount.put(day.getDate1().getYear(),daysCount.get(day.getDate1().getYear())+1);
        }
        if(!containsCurrentYear){
            daysCount.put(LocalDate.now().getYear(), 0);
            years.add(LocalDate.now().getYear());
        }

        model.addAttribute("daysCount",daysCount);
        model.addAttribute("years",years);
        return "years";
    }*/

    @RequestMapping("/years")
    public String courses(Model model) {
        Map<Integer,Integer> years= new HashMap<Integer,Integer>();

        List<Course> courses=courseDAO.findAll();

        for(Course course:courses){
            years.put(course.getId(),course.getEndDate().getYear());
        }

        model.addAttribute("courses",courses);
        model.addAttribute("years",years);
        return "years";
    }


    @RequestMapping("/years/{courseId}")
    public String setCourse(Model model, @PathVariable int courseId, HttpServletResponse response/*,@CookieValue(value = "foo", defaultValue = "hello") String fooCookie*/) {
        Cookie cookie=new Cookie("currentCourseId",courseId+"");
        cookie.setPath("/");
        response.addCookie(cookie);
        //this.year=year;
        return "redirect:/index";
    }

    @RequestMapping("/statistics")
    public String statistics( ) {
        return "statistics";
    }

    @RequestMapping("/about")
    public String about( ) {
        return "about";
    }

    /*public static int getYear(){
        if(year==-1)
            year=LocalDate.now().getYear();
        return year;
    }*/

    @RequestMapping("/settings")
    public String settingsGet(Model model) {

        SettingsForm settingsForm=new SettingsForm(Setting.getSetting());

        model.addAttribute("settingsForm",settingsForm);
        //model.addAttribute("settings",);
        return "setting";
    }

    @PostMapping("/saveSettings")
//    @ResponseBody
    public String saveSettings(@ModelAttribute SettingsForm settingsForm) throws Exception {
        settingsForm.assignData(Setting.getSetting());
        Setting.getSetting().save();
        return "redirect:/index";
    }

    @RequestMapping("/change_password")
    @ResponseBody
    public String change_password(Model model, @RequestParam("oldPW") String oldPW , @RequestParam("newPW") String newPW , @RequestParam("newPW2") String newPW2) {
        Setting setting=Setting.getSetting();
        if(!setting.isSameToPassword(oldPW)){
            return "2";
        }
        if(!newPW.equals(newPW2)){
            return "3";
        }
        setting.setPassword(newPW);
        setting.save();
        return "1";
    }

    @RequestMapping("/exit")
    @ResponseBody
    public String exit( ) {
        System.exit(0);
        return "تم الخروج";
    }
//------------------------ version2 -----------------------------------------------------------------------------------------


        //@CookieValue("currentCourseId") Integer currentCourseId
    private void setCurrentCourseId(HttpServletResponse response,CourseDAO courseDAO2){
        LocalDate toDay=LocalDate.now();
        //toDay=LocalDate.of(2020,1,1);
                Course course=courseDAO2.findByStartDateBeforeAndEndDateAfterOrEndDateOrStartDate(toDay,toDay,toDay,toDay);
                if(course==null){
                    course=createNewCourse("year: "+toDay.getYear(),LocalDate.of(toDay.getYear(),1,1),LocalDate.of(toDay.getYear(),12,31),courseDAO2);
                }
        Cookie cookie=new Cookie("currentCourseId",course.getId()+"");
        cookie.setPath("/");
                response.addCookie(cookie);
    }

    private Course createNewCourse(String title,LocalDate startDate,LocalDate endDate,CourseDAO courseDAO2){
        Course prevCourse=courseDAO.getResentCourse();
        Course newCourse=new Course(title,startDate,endDate);
        courseDAO2.save(newCourse);

        Group newGroup;
        Student newStudent;
        if(prevCourse!=null)
        for(Group group:prevCourse.getGroups()){
            newGroup=new Group(newCourse,group.getStableGroup());
            newCourse.getGroups().add(newGroup);
            for(Student student:group.getStudents()){
                newStudent=new Student(student.getStableStudent(),newCourse,newGroup);
                studentDAO.save(newStudent);
            }
            groupDAO.save(newGroup);
        }
        return newCourse;
    }
//---------------------------------------file archive--------------------------------------------------
@RequestMapping("/fileArchive")
public String fileArchiveGet(Model model,@CookieValue("currentCourseId") Course currentCourse) {
    Iterable<FileArchive> list =currentCourse.getFileArchives();
    String date=LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    model.addAttribute("files", list);
    model.addAttribute("date", date);
    model.addAttribute("title",currentCourse.getTitle());
    return "filesList";
}

    @RequestMapping("/fileArchive/add")
    public String addGet(Model model,@CookieValue("currentCourseId") Integer currentCourseId) {
        model.addAttribute("fileArchive", new FileArchive());
        return "fileArchive-add";
    }

    @PostMapping("/fileArchive/add")
//    @ResponseBody
    public String fileArchivePost(@ModelAttribute FileArchive fileArchive, @RequestParam("file") MultipartFile file, @CookieValue("currentCourseId") Course currentCourse) throws Exception {

        fileArchive.setDate1(LocalDate.now());
        fileArchive.setExtension(fileArchive.extractExtension(file.getOriginalFilename()));
        fileArchive.setCourse(currentCourse);
        fileArchiveDAO.save(fileArchive);


        //save file
        String path=GeneralController.staticPath+"fileArchive\\"+fileArchive.logicalName();
        File f=new File(path);
        file.transferTo(f);
        return "redirect:/fileArchive";
    }

    @RequestMapping("/fileArchive/open/{fileArchive}")
    @ResponseBody
    public String fileArchiveOpen(Model model,@PathVariable FileArchive fileArchive,@CookieValue("currentCourseId") Integer currentCourseId) {
        String path=GeneralController.staticPath+"fileArchive\\"+fileArchive.logicalName();
        File file = new File(path);
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "1";
    }

    @RequestMapping("/fileArchive/delete/{fileArchive}")
    @ResponseBody
    public String fileArchiveDelete(Model model,@PathVariable FileArchive fileArchive,@CookieValue("currentCourseId") Integer currentCourseId) {
        String path=GeneralController.staticPath+"fileArchive\\"+fileArchive.logicalName();
        File file = new File(path);
        if(file.delete())
        fileArchiveDAO.delete(fileArchive);
        return "1";
    }





}
