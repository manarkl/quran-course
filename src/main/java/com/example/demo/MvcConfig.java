package com.example.demo;

import com.example.demo.controllers.GeneralController;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.File;

/**
 * Created by MANAR on 14/05/2018.
 */
@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        File currentDirFile = new File("");
        String path = currentDirFile.getAbsolutePath();
        /*try {
            String currentDir = helper.substring(0, helper.length() - currentDirFile.getCanonicalPath().length());
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        registry.addResourceHandler("/images/**")
                .addResourceLocations("file:"+path+ "/data/images/");

        registry.addResourceHandler("/fileArchive/**")
                .addResourceLocations("file:"+path+ "/data/fileArchive/");

        registry.addResourceHandler("**")
                .addResourceLocations("file:"+path+ "/static/");

        GeneralController.staticPath=path+ "/data/";

        /*registry.addResourceHandler("/images*//**")
                .addResourceLocations(path+"static2/images/");
         registry
         .addResourceHandler("/resources/**")
         .addResourceLocations("/resources/");

         */

    }
}