/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "FileArchive")
public class FileArchive {


    private Integer id;
    private LocalDate date1;
    private String name;
    private String extension;
    private String description;

    private Course course;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }

    public FileArchive() {
    }

    public FileArchive(LocalDate date1, String name, String extension, String description, Course course) {
        this.date1 = date1;
        this.name = name;
        this.extension = extension;
        this.description = description;
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne
    @JoinColumn(name = "course_id")
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String fullName(){
        return name+"."+extension;
    }
    public String logicalName(){
        return id+"."+extension;
    }

    public String extractExtension(String fullName){
        int index=fullName.length()-1;
        //char x=fullName.charAt(index);
        while ((index>0)&&(fullName.charAt(index)!='.')){
            index--;
        }
        index++;
        if((index>0)&&(index<fullName.length()))
            return fullName.substring(index);
        return "";
    }
}
