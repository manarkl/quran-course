/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;

import javax.persistence.*;
import java.util.List;

/**
 * select * form event e where group_id=1 and activity_id=2 and date1=2-3-2003
 * @author MANAR
 */
@Entity
@Table(name = "stablegroup")
public class StableGroup {

    private Integer id;
    //@NotNull
    private String name;
    private List<Group> groups;

    @OneToMany(mappedBy = "stableGroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StableGroup(String name) {
        this.name = name;
    }

    public StableGroup( ) {
    }
    public StableGroup(Integer id) {
        this.id=id;
    }

    public String toString(){
        return this.getName() + " from ";
    }
}

