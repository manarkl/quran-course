package com.example.demo.models;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import com.example.demo.forms.StudentForm;
import com.example.demo.repository.CheckStudentDAO;
import com.example.demo.repository.PageDAO;
import com.example.demo.repository.PartDAO;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "stablestudent")
public class StableStudent {

    private Integer id;
    private String name;
    private String class1;
    private String phone;
    private String facebook;
    private String father;
    private String address;
    private String brothers;
    private String talents;
    private Set<Part> parts;
    private Set<Page> pages;


    @OneToMany(mappedBy = "stableStudent", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<Page> getPages() {
        return pages;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    @OneToMany(mappedBy = "stableStudent", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<Part> getParts() {
        return parts;
    }

    public void setParts(Set<Part> parts) {
        this.parts = parts;
    }

    @Column
    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    @Column
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column
    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    @Column
    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    @Column
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column
    public String getBrothers() {
        return brothers;
    }

    public void setBrothers(String brothers) {
        this.brothers = brothers;
    }

    @Column
    public String getTalents() {
        return talents;
    }

    public void setTalents(String talents) {
        this.talents = talents;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StableStudent() {
    }

    public StableStudent(Integer id) {
        this.id=id;
    }

    public StableStudent(String name) {
        this.name = name;
    }

    public String toString(){
        return this.getName() + " from ";
    }

    public void setData(StudentForm studentForm) {
          name=studentForm.getName();
          class1=studentForm.getClass1();
          phone=studentForm.getPhone();
          facebook=studentForm.getFacebook();
          father=studentForm.getFather();
          address=studentForm.getAddress();
          brothers=studentForm.getBrothers();
          talents=studentForm.getTalents();
    }




    //--------------------------awqaf parts-----------------------------------
    private List<String> partsPerMonth=null;//parts numbers in every month
    private int lastParts;
    public void initAllPartsString(byte type,PartDAO partDAO, int year){
        partsPerMonth=new ArrayList<String>();
        String str="";
        LocalDate date;
        List<Part> parts;
        for(int i=1;i<=12;i++){
            date=LocalDate.of(year,i,1);
            parts=partDAO.findAllByType1AndStableStudentAndDate1BetweenOrderByNumAsc(type,this,date,date.plusMonths(1));
            str=(parts.size()>0)?parts.get(0).getNum()+"":"";
            for (int j=1;j<parts.size();j++){
                str+="-"+parts.get(j).getNum();
            }
            partsPerMonth.add(str);
        }

        parts=partDAO.findAllByType1AndStableStudentAndDate1Before(type,this,LocalDate.of(year,1,1));
        lastParts=parts.size();
    }

    public String partsString(int type,int index){
        return partsPerMonth.get(index);
    }
    public int lastParts(){
        return lastParts;
    }

    //-------------studentShow parts------------------------------------------------------
    List<Part> awqafParts1;
    private String readParts="no data";
    private String absentParts="no data";

    public void initShowStudentPartsString(PartDAO partDAO){
        awqafParts1=partDAO.findAllByType1AndStableStudentOrderByNumAsc((byte)1,this);

        //readParts
        List<Part> parts=partDAO.findAllByType1AndStableStudentOrderByNumAsc((byte)2,this);
        readParts=(parts.size()>0)?parts.get(0).getNum()+"":"";
        for (int j=1;j<parts.size();j++){
            readParts+="-"+parts.get(j).getNum();
        }
        //absentParts
        parts=partDAO.findAllByType1AndStableStudentOrderByNumAsc((byte)3,this);
        absentParts=(parts.size()>0)?parts.get(0).getNum()+"":"";
        for (int j=1;j<parts.size();j++){
            absentParts+="-"+parts.get(j).getNum();
        }
    }

    public List<Part> awqafParts(){return awqafParts1;}
    public String readPartsString(){return readParts;}
    public String absentPartsString(){return absentParts;}

//--------------------pages count-----------------------------------------------

    public int pagesCount() {
        return this.getPages().size();
        //return pageDAO.findAllByStableStudentAndDate1Between(this, startDate, endDate).size();
    }
}
