/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import com.example.demo.Setting;
import com.example.demo.forms.PageForm;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "Page")
public class Page {
    public static final Byte READ_TYPE = 1;
    public static final Byte ABSENT_TYPE = 2;

    //public static final Byte GOOD_EVALUATE = 1;
    public static final Byte VERYGOOD_EVALUATE = 2;
    public static final Byte EXCELLENT_EVALUATE = 3;


    private Integer id;
    private byte num;
    private LocalDate date1;
    private byte type1;//read, unread
    private byte evaluate;// 2-VG, 3-Excellent
    private StableStudent stableStudent;
    private PointAmmount pointAmmount;

    public byte getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(byte evaluate) {
        this.evaluate = evaluate;
    }

    @ManyToOne
    @JoinColumn(name = "stablestudent_id")
    public StableStudent getStableStudent() {
        return stableStudent;
    }

    public void setStableStudent(StableStudent stableStudent) {
        this.stableStudent = stableStudent;
    }


    @Column
    public byte getNum() {
        return num;
    }

    public void setNum(byte num) {
        this.num = num;
    }
    @Column
    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }
    @Column
    public byte getType1() {
        return type1;
    }

    public void setType1(byte type1) {
        this.type1 = type1;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "pointAmount_id")
    public PointAmmount getPointAmmount() {
        return pointAmmount;
    }

    public void setPointAmmount(PointAmmount pointAmmount) {
        this.pointAmmount = pointAmmount;
    }

    public Page() {
    }

    public void setData(PageForm pageForm,StableStudent stableStudent, PointAmmount pointAmmount, Setting setting) {
          num=Byte.parseByte(pageForm.getNum());
          type1=pageForm.getType1();//read, unread
          evaluate=pageForm.getEvaluate();// 1-G, 2-VG, 3-Excellent

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //convert String to LocalDate
        date1 = LocalDate.parse(pageForm.getDate2(), formatter);
        this.stableStudent=stableStudent;

        this.pointAmmount=pointAmmount;
        int amount=0;
        if(pageForm.getType1()==Page.ABSENT_TYPE){
            if(pageForm.getEvaluate()==Page.VERYGOOD_EVALUATE)
                amount=setting.getPageAbsentGood();
            else if(pageForm.getEvaluate()==Page.EXCELLENT_EVALUATE)
                amount=setting.getPageAbsentExcellent();
        }else if(pageForm.getType1()==Page.READ_TYPE){
            if(pageForm.getEvaluate()==Page.VERYGOOD_EVALUATE)
                amount=setting.getPageReadGood();
            else if(pageForm.getEvaluate()==Page.EXCELLENT_EVALUATE)
                amount=setting.getPageReadExcellent();
        }
        this.pointAmmount.setAmmount(amount);
    }
}
