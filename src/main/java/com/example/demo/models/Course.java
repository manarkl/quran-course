/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "course")
public class Course {


    private Integer id;
    private String title="no title";
    private LocalDate startDate;
    private LocalDate endDate;

    private List<Group> groups;
    private List<Activity> activities;
    private List<CheckDay> checkDays;
    private List<Student> students;
    private List<FileArchive> fileArchives;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public Course() {
    }
    public Course(Integer id) {
        this.id=id;
    }

    public Course( String title,LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.title = title;
        groups=new LinkedList<Group>();
    }

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endtDate) {
        this.endDate = endtDate;
    }

    @Column
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /*@Column
    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }*/

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<CheckDay> getCheckDays() {
        return checkDays;
    }

    public void setCheckDays(List<CheckDay> checkDays) {
        this.checkDays = checkDays;
    }

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<FileArchive> getFileArchives() {
        return fileArchives;
    }

    public void setFileArchives(List<FileArchive> fileArchives) {
        this.fileArchives = fileArchives;
    }
}
