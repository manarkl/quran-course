/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 *
 * @author MANAR
 */
@Entity
@Table
public class Activity {


    private Integer id;
    private String name;
    private Course course;
    private Set<EventActivity> eventActivitys;

    @ManyToOne
    @JoinColumn(name = "course_id")
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @OneToMany(mappedBy = "activity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<EventActivity> getEventActivitys() {
        return eventActivitys;
    }

    public void setEventActivitys(Set<EventActivity> eventActivitys) {
        this.eventActivitys = eventActivitys;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Activity(String name,Course course) {
        this.name=name;
        this.course=course;
    }
    public Activity() {
    }
}
