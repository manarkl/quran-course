/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "checkday")
public class CheckDay {


    private Integer id;
    private LocalDate date1;

    private Set<CheckStudent> checkStudents;
    private Course course;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }

    @OneToMany(mappedBy = "checkDay", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<CheckStudent> getCheckStudents() {
        return checkStudents;
    }

    public void setCheckStudents(Set<CheckStudent> checkStudents) {
        this.checkStudents = checkStudents;
    }

    public CheckDay() {
    }

    public CheckDay(LocalDate date1,Course course) {
        this.date1 = date1;
        this.course=course;
    }

    @ManyToOne
    @JoinColumn(name = "course_id")
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
