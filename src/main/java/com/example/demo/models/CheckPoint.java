/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import com.example.demo.Setting;
import com.example.demo.repository.PointAmmountDAO;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "CheckPoint")
public class CheckPoint {


    private Integer id;
    private int points;
    private Student student;
    private LocalDate date1;


    public CheckPoint() {
    }


    public CheckPoint(int points, Student student, LocalDate date1) {
        this.points = points;
        this.student = student;
        this.date1 = date1;
    }

    @ManyToOne
    @JoinColumn(name = "student_id")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column
    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }

    public String printablePoints(){
        return (points<0)?"":points+" نقطة";
    }
}
