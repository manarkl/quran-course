/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


//import com.example.demo.ListWithTag;
import com.example.demo.repository.CheckStudentDAO;
import com.example.demo.repository.EventActivityDAO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * select * form event e where group_id=1 and activity_id=2 and date1=2-3-2003
 * @author MANAR
 */
@Entity// (name = "mygroup")
@Table(name = "mygroup")
public class Group {



    private Integer id;
    //@NotNull
    private StableGroup stableGroup;
    private List<Student> students;
    private Set<EventActivity> eventActivities ;
    private Course course;

    public Group(Course course,StableGroup stableGroup) {
        this.stableGroup=stableGroup;
        this.course=course;
    }

    public Integer stableId(){
        return getStableGroup().getId();
    }

    @ManyToOne
    @JoinColumn(name = "stablegroup_id")
    public StableGroup getStableGroup() {
        return stableGroup;
    }

    public void setStableGroup(StableGroup stableGroup) {
        this.stableGroup = stableGroup;
    }

    @ManyToOne
    @JoinColumn(name = "course_id")
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<EventActivity> getEventActivities() {
        return eventActivities;
    }

    public void setEventActivities(Set<EventActivity> eventActivities) {
        this.eventActivities = eventActivities;
    }

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    @OrderBy("name ASC")
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return getStableGroup().getName();
    }

    public void setName(String name) {
        //this.getStableGroup().setName(name);
    }

    public Group() {
        course=new Course();
    }

    /*public Group(String name,Integer courseId) {
        this.stableGroup=new StableGroup(name);
        this.course=new Course(courseId);
    }*/

    /*public Group(Course course) {
        this.course=course;
    }*/

    public String toString(){
        return this.getStableGroup().getName() + " from ";
    }

    //-----events------

   /* List<ListWithTag<EventActivity>> eventsLists=null;

    public List<ListWithTag<EventActivity>> eventsLists() {
        return eventsLists;
    }

    public void initEventsLists(EventActivityDAO eventActivityDAO,List<Activity> activities,int year){
        if(eventsLists==null)
            eventsLists=new ArrayList<ListWithTag<EventActivity>>();
        for (int i=0;i<activities.size();i++) {
            List<EventActivity> eventActivities=eventActivityDAO.findAllByActivityAndGroupAndDate1Between(activities.get(i),this, LocalDate.of(year,1,1),LocalDate.of(year,12,31));
            eventsLists.add(new ListWithTag<EventActivity>(eventActivities,String.valueOf(activities.get(i).getId())));
        }
    }*/


//--------------existence rate--------------------------
    public static CheckStudentDAO checkStudentDAO;
    public static List<CheckDay> checkDays;

    public int existenceRat(){
        int avg=0;
        for(Student student:this.getStudents()){
            avg+=student.existenceRat();
        }
        if(this.getStudents().size()==0)
            return 0;

        avg/=this.getStudents().size();
        return avg;
    }

//-------------advance rate--------------------------
    public static int year;

    public int advanceRate(){
        int pagesCount=0;
        int lastPagesCount=0;
        for(Student student:this.getStudents()){
            pagesCount+=student.pagesCount();
            lastPagesCount+=student.pagesCount();
        }
        if(this.getStudents().size()==0)
            return 0;
        if(lastPagesCount==0)
            lastPagesCount=1;

        return (pagesCount*100)/lastPagesCount;
    }
}

