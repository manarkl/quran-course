/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import com.example.demo.Setting;
import com.example.demo.forms.PageForm;
import com.example.demo.repository.PointAmmountDAO;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "pointAmmount")
public class PointAmmount {


    private Integer id;
    private int ammount;
    private Student student;
    private LocalDate date1;

    private Set<Page> pages;
    private Set<Part> parts;
    private Set<CheckStudent> checkStudents;


    public PointAmmount() {
    }

    public PointAmmount(Student student,PointAmmountDAO pointAmmountDAO) {
        this.ammount=0;
        this.date1=LocalDate.now();
        this.student=student;
        pointAmmountDAO.save(this);
    }

    public PointAmmount(int ammount, Student student, LocalDate date1) {
        this.ammount = ammount;
        this.student = student;
        this.date1 = date1;
    }

    @ManyToOne
    @JoinColumn(name = "student_id")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column
    public int getAmmount() {
        return ammount;
    }

    public void setAmmount(int ammount) {
        this.ammount = ammount;
    }

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }


    @OneToMany(mappedBy = "pointAmmount", cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    public Set<Page> getPages() {
        return pages;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    @OneToMany(mappedBy = "pointAmmount", cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    public Set<Part> getParts() {
        return parts;
    }

    public void setParts(Set<Part> parts) {
        this.parts = parts;
    }

    @OneToMany(mappedBy = "pointAmmount", cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    public Set<CheckStudent> getCheckStudents() {
        return checkStudents;
    }

    public void setCheckStudents(Set<CheckStudent> checkStudents) {
        this.checkStudents = checkStudents;
    }

    /*static public1 PointAmmount getPointAmount2(Setting setting, Student student, LocalDate date1, PageForm pageForm){
        int amount=0;
        if(pageForm.getType1()==Page.ABSENT_TYPE){
            if(pageForm.getEvaluate()==Page.VERYGOOD_EVALUATE)
                amount=setting.getPageAbsentGood();
            else if(pageForm.getEvaluate()==Page.EXCELLENT_EVALUATE)
                amount=setting.getPageAbsentExcellent();
        }else if(pageForm.getType1()==Page.READ_TYPE){
            if(pageForm.getEvaluate()==Page.VERYGOOD_EVALUATE)
                amount=setting.getPageReadGood();
            else if(pageForm.getEvaluate()==Page.EXCELLENT_EVALUATE)
                amount=setting.getPageReadExcellent();
        }
        return new PointAmmount(amount,student,date1);
    }*/

    public static PointAmmount getForPart(Student student,byte type){
        int amount=0;
        if(type==Part.absent)
            amount=Setting.getSetting().getPartAbsent();
        else if(type==Part.read)
            amount=Setting.getSetting().getPartRead();
        else
            throw new UnsupportedOperationException("the type is not read nor absent: "+type);

        return new PointAmmount(amount,student,LocalDate.now());
    }
}
