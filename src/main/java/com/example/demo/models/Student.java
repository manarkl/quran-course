package com.example.demo.models;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import com.example.demo.forms.StudentForm;
import com.example.demo.repository.CheckStudentDAO;
import com.example.demo.repository.PageDAO;
import com.example.demo.repository.PartDAO;
import com.example.demo.repository.StudentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "student")
public class Student {

    private Integer id;
    private Group group;
    private StableStudent stableStudent;
    private Set<CheckStudent> checkStudents;
    private Course course;

    public Student(StableStudent stableStudent,Course course,Group group) {
        this.stableStudent=stableStudent;
        this.course=course;
        this.group=group;
    }


    @ManyToOne
    @JoinColumn(name = "course_id")
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }



    @ManyToOne
    @JoinColumn(name = "stableStudent_id")
    public StableStudent getStableStudent() {
        return stableStudent;
    }

    public void setStableStudent(StableStudent stableStudent) {
        this.stableStudent = stableStudent;
    }

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<CheckStudent> getCheckStudents() {
        return checkStudents;
    }

    public void setCheckStudents(Set<CheckStudent> checkStudents) {
        this.checkStudents = checkStudents;
    }


    public Set<Page> tgetPages() {
        return getStableStudent().getPages();
    }

    public void setPages(Set<Page> pages) {
        getStableStudent().setPages(pages);
    }

    @ManyToOne
    @JoinColumn(name = "mygroup_id")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Set<Part> tgetParts() {
        return getStableStudent().getParts();
    }

    public void setParts(Set<Part> parts) {
        getStableStudent().setParts(parts);
    }


    public String getClass1() {
        return getStableStudent().getClass1();
    }

    public void setClass1(String class1) {
        getStableStudent().setClass1(class1);
    }

    public String getPhone() {
        return getStableStudent().getPhone();
    }

    public void setPhone(String phone) {
        getStableStudent().setPhone(phone);
    }


    public String getFacebook() {
        return getStableStudent().getFacebook();
    }

    public void setFacebook(String facebook) {
        getStableStudent().setFacebook(facebook);
    }


    public String getFather() {
        return getStableStudent().getFather();
    }

    public void setFather(String father) {
        getStableStudent().setFather(father);
    }


    public String getAddress() {
        return getStableStudent().getAddress();
    }

    public void setAddress(String address) {
        getStableStudent().setAddress(address);
    }


    public String getBrothers() {
        return getStableStudent().getBrothers();
    }

    public void setBrothers(String brothers) {
        getStableStudent().setBrothers(brothers);
    }


    public String getTalents() {
        return getStableStudent().getTalents();
    }

    public void setTalents(String talents) {
        getStableStudent().setTalents(talents);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer tgetStableId(){
        return getStableStudent().getId();
    }

    public String getName() {
        return getStableStudent().getName();
    }

    public void setName(String name) {
        getStableStudent().setName(name);
    }

    public Student() {
        stableStudent=new StableStudent();
    }
    public Student(int id,String name) {
        stableStudent=new StableStudent();
        this.id=id;
        this.setName(name);
    }

    public Student(Integer id) {
        this.id=id;
    }

    public Student(String name) {
        stableStudent=new StableStudent(name);
    }

    public String toString(){
        return this.getName() + " from ";
    }

    public void setData(StudentForm studentForm,Integer courseId) {
        getStableStudent().setData(studentForm);
        this.course = new Course(courseId);
    }

    public int stableId(){
        return getStableStudent().getId();
    }

    //--------------------------awqaf parts-----------------------------------
    private List<String> partsPerMonth=null;//parts numbers in every month
    private int lastParts;
    public void initAllPartsString(byte type,PartDAO partDAO, int year){
        getStableStudent().initAllPartsString(type,partDAO,year);
    }

    public String partsString(int type,int index){
        return getStableStudent().partsString(type,index);
    }
    public int lastParts(){
        return getStableStudent().lastParts();
    }

    //-------------studentShow parts------------------------------------------------------
    //List<Part> awqafParts;
    //String readParts="no data";
    //String absentParts="no data";

    public void initShowStudentPartsString(PartDAO partDAO){
        getStableStudent().initShowStudentPartsString(partDAO);
    }

    public List<Part> awqafParts(){return stableStudent.awqafParts();}
    public String readPartsString(){return stableStudent.readPartsString();}
    public String absentPartsString(){return stableStudent.absentPartsString();}




    //---------absent students-----------------
    //public static List<CheckDay> checkDays;
    //public static CheckStudentDAO checkStudentDAO;

    public int existenceRat() {
        int checkDaysCount=getCheckStudents().size();
        int allDaysCount=getCourse().getCheckDays().size();
        return (allDaysCount==0)?0:(checkDaysCount*100)/allDaysCount;
    }

    /*public int existenceRat(List<CheckDay> checkDays, CheckStudentDAO checkStudentDAO) {
        this.checkDays=checkDays;
        this.checkStudentDAO=checkStudentDAO;
        return existenceRat();
    }*/

    /*public int pointsCount(){
        if(checkDays.size()==0)
            return 0;

        int pointsCount=0;
        List<CheckStudent> checkStudents;
        for(CheckDay checkDay:checkDays){
            checkStudents= checkStudentDAO.findAllByStudentAndCheckDay(this,checkDay);
            for(CheckStudent checkStudent:checkStudents){

            }
        }
        return (existance*100)/checkDays.size();
    }*/
//--------------------pages count-----------------------------------------------
    //public static PageDAO pageDAO;

    public int pagesCount() {
        return stableStudent.pagesCount();
    }
}
