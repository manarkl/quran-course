/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "checkstudent")
public class CheckStudent {


    private Integer id;

    private Student student;
    private CheckDay checkDay;
    private PointAmmount pointAmmount;

    public CheckStudent(Student student, CheckDay checkDay,PointAmmount p) {
        this.student = student;
        this.checkDay = checkDay;
    }

    public CheckStudent() {
    }

    @ManyToOne
    @JoinColumn(name = "student_id")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @ManyToOne
    @JoinColumn(name = "checkday_id")
    public CheckDay getCheckDay() {
        return checkDay;
    }

    public void setCheckDay(CheckDay checkDay) {
        this.checkDay = checkDay;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "pointAmount_id")
    public PointAmmount getPointAmmount() {
        return pointAmmount;
    }

    public void setPointAmmount(PointAmmount pointAmmount) {
        this.pointAmmount = pointAmmount;
    }
}
