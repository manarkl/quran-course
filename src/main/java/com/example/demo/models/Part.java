/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

/**
 *
 * @author MANAR
 */
@Entity
@Table(name = "part")
public class Part {


    private Integer id;
    private byte num;
    private LocalDate date1;
    private byte type1;//1- awqaf 2-read 3-absent 4-honoring
    private StableStudent stableStudent;
    private PointAmmount pointAmmount;

    public static final Byte awqaf = 1;
    public static final Byte read = 2;
    public static final Byte absent = 3;
    public static final Byte honoring = 4;


    @ManyToOne
    @JoinColumn(name = "stablestudent_id")
    public StableStudent getStableStudent() {
        return stableStudent;
    }

    public void setStableStudent(StableStudent stableStudent) {
        this.stableStudent = stableStudent;
    }



    @Column
    public byte getNum() {
        return num;
    }

    public void setNum(byte num) {
        this.num = num;
    }
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public LocalDate getDate1() {
        return date1;
    }

    public void setDate1(LocalDate date1) {
        this.date1 = date1;
    }
    @Column
    public byte getType1() {
        return type1;
    }

    public void setType1(byte type1) {
        this.type1 = type1;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "pointAmount_id")
    public PointAmmount getPointAmmount() {
        return pointAmmount;
    }

    public void setPointAmmount(PointAmmount pointAmmount) {
        this.pointAmmount = pointAmmount;
    }

    public Part() {
    }

    public Part(byte num, LocalDate date1, byte type1, StableStudent stableStudent,PointAmmount pointAmmount) {
        this.num = num;
        this.date1 = date1;
        this.type1 = type1;
        this.stableStudent = stableStudent;
        this.pointAmmount=pointAmmount;
    }
}
