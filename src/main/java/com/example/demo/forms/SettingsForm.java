package com.example.demo.forms;

import com.example.demo.Setting;



/**
 * Created by MANAR on 14/04/2018.
 */
public class SettingsForm {
    private int dayPoints;
    private int pageReadGood;
    private int pageReadExcellent;
    private int pageAbsentGood;
    private int pageAbsentExcellent;
    private int partRead;
    private int partAbsent;
    private int partAwqaf;

    public SettingsForm() {
    }

    public SettingsForm(Setting setting) {
        this.dayPoints = setting.getDayPoints();
        this.pageReadGood = setting.getPageReadGood();
        this.pageReadExcellent = setting.getPageReadExcellent();
        this.pageAbsentGood = setting.getPageAbsentGood();
        this.pageAbsentExcellent = setting.getPageAbsentExcellent();
        this.partRead = setting.getPartRead();
        this.partAbsent = setting.getPartAbsent();
        this.partAwqaf = setting.getPartAwqaf();
    }

    public int getDayPoints() {
        return dayPoints;
    }

    public void setDayPoints(int dayPoints) {
        this.dayPoints = dayPoints;
    }

    public int getPageReadGood() {
        return pageReadGood;
    }

    public void setPageReadGood(int pageReadGood) {
        this.pageReadGood = pageReadGood;
    }

    public int getPageReadExcellent() {
        return pageReadExcellent;
    }

    public void setPageReadExcellent(int pageReadExcellent) {
        this.pageReadExcellent = pageReadExcellent;
    }

    public int getPageAbsentGood() {
        return pageAbsentGood;
    }

    public void setPageAbsentGood(int pageAbsentGood) {
        this.pageAbsentGood = pageAbsentGood;
    }

    public int getPageAbsentExcellent() {
        return pageAbsentExcellent;
    }

    public void setPageAbsentExcellent(int pageAbsentExcellent) {
        this.pageAbsentExcellent = pageAbsentExcellent;
    }

    public int getPartRead() {
        return partRead;
    }

    public void setPartRead(int partRead) {
        this.partRead = partRead;
    }

    public int getPartAbsent() {
        return partAbsent;
    }

    public void setPartAbsent(int partAbsent) {
        this.partAbsent = partAbsent;
    }

    public int getPartAwqaf() {
        return partAwqaf;
    }

    public void setPartAwqaf(int partAwqaf) {
        this.partAwqaf = partAwqaf;
    }

    public void assignData(Setting setting) {
        setting.setDayPoints(dayPoints);
        setting.setPageReadGood ( pageReadGood);
        setting.setPageReadExcellent ( pageReadExcellent);
        setting.setPageAbsentGood ( pageAbsentGood);
        setting.setPageAbsentExcellent ( pageAbsentExcellent);
        setting.setPartRead ( partRead);
        setting.setPartAbsent ( partAbsent);
        setting.setPartAwqaf ( partAwqaf);
    }
}
