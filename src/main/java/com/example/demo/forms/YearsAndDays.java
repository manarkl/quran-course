package com.example.demo.forms;

/**
 * Created by MANAR on 07/05/2018.
 */
public class YearsAndDays {
    private int year;
    private int daysCount;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }
}
