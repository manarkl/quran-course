package com.example.demo.forms;

import com.example.demo.models.Part;

import java.util.Set;

/**
 * Created by MANAR on 14/04/2018.
 */
public class StudentForm {
    private String name;
    private String class1;
    private String phone;
    private String facebook;
    private String father;
    private String address;
    private String brothers;
    private String talents;
    private String groupId;

    private Set<String> absentParts;
    private Set<String> readParts;
    private Set<String> awqafParts;

    public Set<String> getAbsentParts() {
        return absentParts;
    }

    public void setAbsentParts(Set<String> absentParts) {
        this.absentParts = absentParts;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBrothers() {
        return brothers;
    }

    public void setBrothers(String brothers) {
        this.brothers = brothers;
    }

    public String getTalents() {
        return talents;
    }

    public void setTalents(String talents) {
        this.talents = talents;
    }



    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return this.getName() + " from ";
    }

    public Set<String> getReadParts() {
        return readParts;
    }

    public void setReadParts(Set<String> readParts) {
        this.readParts = readParts;
    }

    public Set<String> getAwqafParts() {
        return awqafParts;
    }

    public void setAwqafParts(Set<String> awqafParts) {
        this.awqafParts = awqafParts;
    }
}
