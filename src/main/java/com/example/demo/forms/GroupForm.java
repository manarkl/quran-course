package com.example.demo.forms;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by MANAR on 14/04/2018.
 */
public class GroupForm {
    private String name;
    //private String path;
    /*private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }*/

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return this.getName() + " from ";
    }
}
