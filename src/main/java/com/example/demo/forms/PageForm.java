/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.forms;


import com.example.demo.models.Student;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author MANAR
 */

public class PageForm {

    private String num;
    //private Date date1;
    private byte type1;//1- read, 2- unread
    private byte evaluate;// 1-G, 2-VG, 3-Excellent
    private Student student;
    private String date2;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public byte getType1() {
        return type1;
    }

    public void setType1(byte type1) {
        this.type1 = type1;
    }

    public byte getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(byte evaluate) {
        this.evaluate = evaluate;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String toString(){
        return String.format("page /%s form",num);
    }
}
