package com.example.demo;

import com.example.demo.helpers.FileHandler;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by MANAR on 16/05/2018.
 */
public class Setting implements Serializable {

    public static void main(String[] args){
        generateNewFile();
    }
    private static void generateNewFile(){
        Setting NewSetting=new Setting(toDigits("0000"),0,0,0,0,0,0,0,0);
        NewSetting.save();
    }

    //singlton

    private int dayPoints;
    private int pageReadGood;
    private int pageReadExcellent;
    private int pageAbsentGood;
    private int pageAbsentExcellent;
    private String p;//password
    private int partRead;
    private int partAbsent;
    private int partAwqaf;

    private static Setting  setting=null;
    private static final String fileName="setting";

    public static Setting getSetting() {
        if(setting==null)
            try {
                /*if(!FileHandler.isFileExist(fileName))
                    generateNewFile();*/
                    setting=(Setting) FileHandler.readObjectFromFile(fileName);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        return setting;
    }

    public void setData(int dayPoints, int pageReadGood, int pageReadExcellent, int pageAbsentGood, int pageAbsentExcellent, int partRead, int partAbsent, int partAwqaf) {
        this.dayPoints = dayPoints;
        this.pageReadGood = pageReadGood;
        this.pageReadExcellent = pageReadExcellent;
        this.pageAbsentGood = pageAbsentGood;
        this.pageAbsentExcellent = pageAbsentExcellent;
        this.partRead = partRead;
        this.partAbsent = partAbsent;
        this.partAwqaf = partAwqaf;
    }

    public void save(){
        FileHandler.writeToFile(fileName, this);
    }

    private Setting() {
    }

    private static String toDigits(String password){
        String str="";
        for(int c:password.toCharArray()){
            str+=String.valueOf(c);
        }
        return str;
    }

    private Setting(String p, int dayPoints, int pageReadGood, int pageReadExcellent, int pageAbsentGood, int pageAbsentExcellent, int partRead, int partAbsent, int partAwqaf) {
        this.p=p;
        this.dayPoints = dayPoints;
        this.pageReadGood = pageReadGood;
        this.pageReadExcellent = pageReadExcellent;
        this.pageAbsentGood = pageAbsentGood;
        this.pageAbsentExcellent = pageAbsentExcellent;
        this.partRead = partRead;
        this.partAbsent = partAbsent;
        this.partAwqaf = partAwqaf;
    }

    public boolean isSameToPassword(String password) {
        return p.equals(toDigits(password));
    }

    public void setPassword(String password) {
        this.p=toDigits(password);
    }

    public int getDayPoints() {
        return dayPoints;
    }

    public void setDayPoints(int dayPoints) {
        this.dayPoints = dayPoints;
    }

    public int getPageReadGood() {
        return pageReadGood;
    }

    public void setPageReadGood(int pageReadGood) {
        this.pageReadGood = pageReadGood;
    }

    public int getPageReadExcellent() {
        return pageReadExcellent;
    }

    public void setPageReadExcellent(int pageReadExcellent) {
        this.pageReadExcellent = pageReadExcellent;
    }

    public int getPageAbsentGood() {
        return pageAbsentGood;
    }

    public void setPageAbsentGood(int pageAbsentGood) {
        this.pageAbsentGood = pageAbsentGood;
    }

    public int getPageAbsentExcellent() {
        return pageAbsentExcellent;
    }

    public void setPageAbsentExcellent(int pageAbsentExcellent) {
        this.pageAbsentExcellent = pageAbsentExcellent;
    }

    public int getPartRead() {
        return partRead;
    }

    public void setPartRead(int partRead) {
        this.partRead = partRead;
    }

    public int getPartAbsent() {
        return partAbsent;
    }

    public void setPartAbsent(int partAbsent) {
        this.partAbsent = partAbsent;
    }

    public int getPartAwqaf() {
        return partAwqaf;
    }

    public void setPartAwqaf(int partAwqaf) {
        this.partAwqaf = partAwqaf;
    }
}
