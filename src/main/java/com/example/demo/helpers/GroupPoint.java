package com.example.demo.helpers;

/**
 * Created by MANAR on 29-Dec-2018.
 */
public class GroupPoint {

    int id;
    String groupName;
    int points;

    public GroupPoint(int id, String groupName, int points) {
        this.id = id;
        this.groupName = groupName;
        this.points = points;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
