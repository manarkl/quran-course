package com.example.demo.helpers;

import com.example.demo.models.Activity;
import com.example.demo.models.EventActivity;
import com.example.demo.models.Group;
import com.example.demo.repository.EventActivityDAO;

import java.util.List;

/**
 * Created by MANAR on 02-Jan-2019.
 */
public class ActivityGroupCount {
    private EventActivityDAO eventActivityDAO;

    public ActivityGroupCount(EventActivityDAO eventActivityDAO) {
        this.eventActivityDAO=eventActivityDAO;
    }

    public int eventsCount(Group group, Activity activity){
        List<EventActivity> eventActivities=eventActivityDAO.findAllByGroupAndActivity(group,activity);
        return eventActivities.size();
    }
}
