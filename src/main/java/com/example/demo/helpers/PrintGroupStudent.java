package com.example.demo.helpers;

import com.example.demo.models.Course;
import com.example.demo.models.Page;
import com.example.demo.models.Student;
import com.example.demo.repository.CheckPointDAO;
import com.example.demo.repository.PageDAO;
import com.example.demo.repository.PointAmmountDAO;

import java.time.LocalDate;

/**
 * Created by m.al-kul on 2019-03-03.
 */
public class PrintGroupStudent {

    int availablePoints=-1;
    int paidPoints=-1;
    int weekpages=-1;
    int monthPages=-1;
    int allPages=-1;

    Student student;
    static LocalDate now;

    public PrintGroupStudent(Student student, PointAmmountDAO pointAmmountDAO, CheckPointDAO checkPointDAO, PageDAO pageDAO) {
        this.student = student;
        int allPoints= pointAmmountDAO.getPointsCount(student);
        paidPoints=checkPointDAO.getPointsCount(student);
        availablePoints=allPoints-paidPoints;

        this.now=LocalDate.now();
        LocalDate startOfWeek=firstOfWeek(now);
        LocalDate startOfMonth=firstOfMonth(now);

        weekpages=pageDAO.countAllByStableStudentAndType1AndDate1Between(student.getStableStudent(),Page.ABSENT_TYPE,startOfWeek,now);
        monthPages=pageDAO.countAllByStableStudentAndType1AndDate1Between(student.getStableStudent(),Page.ABSENT_TYPE,startOfMonth,now);
        allPages= pageDAO.countAllByStableStudentAndType1AndDate1Between(student.getStableStudent(),Page.ABSENT_TYPE,student.getCourse().getStartDate(),now);
    }


    public PrintGroupStudent(Student student, PointAmmountDAO pointAmmountDAO, CheckPointDAO checkPointDAO) {
        this.student = student;
        int allPoints= pointAmmountDAO.getPointsCount(student);
        paidPoints=checkPointDAO.getPointsCount(student);
        availablePoints=allPoints-paidPoints;
        this.now=LocalDate.now();
    }

    public PrintGroupStudent(Student student1) {
        student=student1;
        availablePoints=0;
    }

    private LocalDate firstOfMonth(LocalDate now) {
        long seek=now.getDayOfMonth();
        LocalDate first= LocalDate.of(now.getYear(),now.getMonth(),1);
        return first;
    }

    private LocalDate firstOfWeek(LocalDate now) {
        long seek=now.getDayOfWeek().getValue();
        LocalDate first= LocalDate.of(now.getYear(),now.getMonth(),now.getDayOfMonth());
        seek=(seek%7)+1;
        if(seek!=6){
            first= now.minusDays(seek);
        }
        return first;
    }


    public int getId() {
        return student.stableId();
    }

    public String getName() {
        return student.getName();
    }

    public int getAvailablePoints() {
        return availablePoints;
    }

    public int getPaidPoints() {
        return paidPoints;
    }

    public int getWeekpages() {
        return weekpages;
    }

    public int getMonthPages() {
        return monthPages;
    }

    public int getAllPages() {
        return allPages;
    }

    public Student getStudent() {
        return student;
    }
}

