/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.time.LocalDate;

/**
 *
 * @author MANAR
 */
public class FileHandler {

    public static String readFile(String filename) {
        String content = null;
        File file = new File(filename); //for ex foo.txt
        FileReader reader = null;
        try {
            reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public static void writeToFile(String name, String data) throws Exception {
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(name), "utf-8"));
        writer.write(data);
        writer.close();
    }

    public static void writeToFile(String name, Object obj) {
        try {
//        File yourFile = new File(name);
//        yourFile.createNewFile(); // if file already exists will do nothing 
            FileOutputStream oFile = new FileOutputStream(name, false);
            ObjectOutputStream out = new ObjectOutputStream(oFile);
            out.writeObject(obj);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static Object readObjectFromFile(String name) throws IOException, ClassNotFoundException {
        FileInputStream reader=new FileInputStream(name);
        ObjectInputStream in=new ObjectInputStream(reader);
        Object obj=in.readObject();
        in.close();
        return obj;
    }

    public static boolean isFileExist(String fileName){
        return new File(fileName).exists();
    }
}
