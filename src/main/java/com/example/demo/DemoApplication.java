package com.example.demo;

import com.example.demo.frames.Logo;
import com.example.demo.frames.Startup;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;

@SpringBootApplication
public class DemoApplication {
	public static Startup startupFrame;
	public static Logo logo;
	public static void main(String[] args) throws Exception{
		//File file1=new File("mamamia.txt");

		FileOutputStream errFile=new FileOutputStream("errors.txt",true);
		PrintStream errPrintStream=new PrintStream(errFile);
		System.setErr(errPrintStream);

		//PipedOutputStream pipedOutputStream=new PipedOutputStream();
		//PrintStream j=new PrintStream(file);
		//PipedInputStream pipedInputStream=new PipedInputStream(pipedOutputStream);
		//System.setOut(j);

		startupFrame=new Startup(args);
		startupFrame.run();
		//SpringApplication.run(DemoApplication.class, args);
	}

}

