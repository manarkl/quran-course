package com.example.demo.repository;


import com.example.demo.models.Activity;
import com.example.demo.models.EventActivity;
import com.example.demo.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public interface EventActivityDAO extends JpaRepository<EventActivity, Integer> {
        public List<EventActivity> findAllByActivityAndGroupAndDate1Between(Activity activity, Group group, LocalDate startDate,LocalDate endDate);
        public List<EventActivity> findAllByGroupAndActivity(Group group,Activity activity);
}