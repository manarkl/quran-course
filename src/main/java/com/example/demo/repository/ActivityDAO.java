package com.example.demo.repository;


import com.example.demo.models.Activity;
import com.example.demo.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ActivityDAO extends JpaRepository<Activity, Integer> {

}