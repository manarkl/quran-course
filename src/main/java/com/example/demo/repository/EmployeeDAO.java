package com.example.demo.repository;

import javax.transaction.Transactional;

import com.example.demo.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface EmployeeDAO extends JpaRepository<Employee, Long> {


} // class UserDao
