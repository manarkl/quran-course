package com.example.demo.repository;


import com.example.demo.models.Course;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public interface CourseDAO extends JpaRepository<Course, Integer> {
    /*@Query("select c from Course c where c.startDate < :toDay and c.endtDate > :toDay")
    Course tfindByDate(LocalDate toDay);*/

    Course findByStartDateBeforeAndEndDateAfterOrEndDateOrStartDate(LocalDate date,LocalDate date1,LocalDate date3,LocalDate date4);

    //Course getByEndDateMax();
    @Query("select c from Course c where c.endDate in(select max(c2.endDate)from Course c2)")
    Course getResentCourse();
}