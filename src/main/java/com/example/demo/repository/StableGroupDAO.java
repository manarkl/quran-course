package com.example.demo.repository;


import com.example.demo.models.Group;
import com.example.demo.models.StableGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface StableGroupDAO extends JpaRepository<StableGroup, Integer> {
}