package com.example.demo.repository;


import com.example.demo.models.CheckDay;
import com.example.demo.models.CheckStudent;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public interface CheckStudentDAO extends JpaRepository<CheckStudent, Integer> {
        List<CheckStudent> findAllByStudentAndCheckDay(Student student, CheckDay checkDay);
        //List<CheckStudent> findAllByStudent(Student student);
        /*@Query("select sum(s.points) from CheckStudent s where s.student= :student and  s.checkDay in (select c from CheckDay c where c.date1 between :startDate and :endDate)")
        Integer getPointsCount(@Param("student") Student student,@Param("startDate") LocalDate startDate,@Param("endDate") LocalDate endDate);*/


}