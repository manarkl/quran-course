package com.example.demo.repository;


import com.example.demo.forms.YearsAndDays;
import com.example.demo.models.CheckDay;
import com.example.demo.models.Course;
import com.example.demo.models.Part;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.*;
import java.sql.*;

import javax.persistence.*;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public interface CheckDayDAO extends JpaRepository<CheckDay, Integer> {
    @Query("select s from Student s where (s not in (select c.student from CheckStudent c where c.checkDay = :checkDay))and(s.course= :course)")
    List<Student> getAbsentStudents(@Param("checkDay") CheckDay checkDay, @Param("course") Course course);

    //the day (31/12/2018) is set out of work days because it will carry all the points which have no days (to avoid change in database and reconstruct the existing data)
    //List<CheckDay> findAllByDate1Between(LocalDate start,LocalDate end);

    CheckDay findByDate1Is(LocalDate dateTime);

    /*@Query("select count(year) as daysCount , FUNCTION('YEAR',c.date1) as year from CheckDay c group by year ")
    List<YearsAndDays> getyearsAndCount();*/

}