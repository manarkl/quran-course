package com.example.demo.repository;


import com.example.demo.models.CheckPoint;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Transactional
public interface CheckPointDAO extends JpaRepository<CheckPoint, Integer> {
        /*@Query("select sum(c.points) from CheckPoint c where (c.student= :student) and  (c.date1 between :startDate and :endDate)")
        Integer getPointsCount(@Param("student") Student student, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);*/

    @Query("select COALESCE(sum(c.points),0) from CheckPoint c where (c.student= :student)")
    Integer getPointsCount(@Param("student") Student student);


}