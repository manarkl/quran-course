package com.example.demo.repository;


import com.example.demo.models.StableStudent;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface StableStudentDAO extends JpaRepository<StableStudent, Integer> {


}