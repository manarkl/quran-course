package com.example.demo.repository;


import com.example.demo.models.Activity;
import com.example.demo.models.FileArchive;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface FileArchiveDAO extends JpaRepository<FileArchive, Integer> {

}