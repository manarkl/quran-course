package com.example.demo.repository;


import com.example.demo.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface GroupDAO extends JpaRepository<Group, Integer> {
    public List<Group> findAllByCourseIdOrderByIdAsc(Integer courseId);
    public List<Group> findAllByCourseId(Integer courseId);
}