package com.example.demo.repository;


import com.example.demo.models.CheckDay;
import com.example.demo.models.CheckStudent;
import com.example.demo.models.PointAmmount;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public interface PointAmmountDAO extends JpaRepository<PointAmmount, Integer> {
        @Query("select COALESCE(sum(s.ammount),0) from PointAmmount s where (s.student= :student) and  (s.date1 between :startDate and :endDate)")
        Integer getPointsCount(@Param("student") Student student, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

        @Query("select COALESCE(sum(s.ammount),0) from PointAmmount s where (s.student= :student)")
        Integer getPointsCount(@Param("student") Student student);


}