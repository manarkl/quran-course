package com.example.demo.repository;


import com.example.demo.models.Page;
import com.example.demo.models.StableStudent;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public interface PageDAO extends JpaRepository<Page, Integer> {
    //List<Page> findAllByStableStudentAndDate1Between(StableStudent stableStudent,LocalDate startDate, LocalDate endDate);

    int countAllByStableStudentAndType1AndDate1Between(StableStudent stableStudent, Byte type1, LocalDate startDate, LocalDate endDate);
    List<Page> findAllByType1AndNumAndStableStudent(byte type,byte num, StableStudent stableStudent);
}