package com.example.demo.repository;


import com.example.demo.models.Page;
import com.example.demo.models.Part;
import com.example.demo.models.StableStudent;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
public interface PartDAO extends JpaRepository<Part, Integer> {
    List<Part> findAllByType1AndStableStudentAndDate1BetweenOrderByNumAsc(Byte type, StableStudent stableStudent, LocalDate startDate, LocalDate endDate);
    List<Part> findAllByType1AndStableStudentAndDate1Before(byte type,StableStudent stableStudent,LocalDate date);
    List<Part> findAllByType1AndStableStudentOrderByNumAsc(Byte type, StableStudent stableStudent);

    public List<Part> findAllByType1AndNumAndStableStudent(byte type,byte num,StableStudent stableStudent);
}