package com.example.demo.repository;


import com.example.demo.models.Group;
import com.example.demo.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface StudentDAO extends JpaRepository<Student, Integer> {
    Student findByStableStudentIdAndCourseId(Integer StableStudentId,Integer CourseId);

    List<Student> findAllByCourseId(Integer CourseId);

}